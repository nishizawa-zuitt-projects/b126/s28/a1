const Course = require("../models/course");

//Create courses
module.exports.createCourse = (body) => {
	let newCourse = new Course({
		name: body.name,
		description: body.description,
		price: body.price
	})

	return newCourse.save().then((course,error) =>{
		if(error){
			return false; // course was NOT saved
		}else {
			return true; // course was successfully saved
		}
	})
}

//get all courses
module.exports.getCourses =()=>{
	return Course.find({}).then(result =>{
		return result// find all courses, then return the result
	})
}

//get specific course
module.exports.getCourse = (params)=>{
	// findById is a Mongoose operation that just finds a document by its ID
	return Course.findById(params.courseId).then(result =>{
		return result
	})
}

// update specific course
module.exports.updateCourse = (params, body) => {
	let updatedCourse = {
		name:body.name,
		description: body.description,
		price: body.price
	}

	return Course.findByIdAndUpdate(params.courseId, updatedCourse).then((course,err) =>{
		if(err){
			return false
		}else{
			return true
		}
	})
}

module.exports.archiveCourse = (params) =>{
	let archivedCourse = {
		isActive: false
	}

	return Course.findByIdAndUpdate(params.courseId, archivedCourse).then((course, err)=>{
		if(err){
			return false
		}else{
			return true
		}
	})
}








