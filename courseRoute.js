const express = require('express')
const router = express.Router()
const courseController = require("../controllers/course")
const auth = require("../auth")

//route to create a new course
//auth.verify below is what is called "Middleware"
router.post("/", auth.verify, (req, res)=>{
	//Activity instruction:
	//Create a route + controller function to create a new course,
	//complete with a proper response from the server once the operation resolves
	// Make sure to use Postman to send a request to test/create your course

	console.log(auth.decode(req.headers.authorization))
	/*10-min Mini Activity
	Create your own way to make sure that ONLY admins can create a new course
	If a non-admin tries, they simply receive an error message saying {auth: failed}
	Make sure to test all cases in Postman

	auth.decode turns out token into a decoded javascript object that we can use
	the dot notation on to access its properties (such as isAdmin)
	*/
	if (auth.decode(req.headers.authorization).isAdmin){
		courseController.createCourse(req.body).then(resultFromController => res.send(resultFromController))
	}else{
		return res.send({auth: "failed"})
	}
})

//route for getting all active courses
router.get("/", (req, res)=> {
	console.log(req)
	courseController.getCourses().then(resultFromController => res.send(resultFromController))
})

//route for getting a specific course
router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})


//update existing course auth.verify is called "mid ware"
router.put("/:courseId", auth.verify, (req, res) =>{
	if (auth.decode(req.headers.authorization).isAdmin){
		courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
	}else{
		return res.send({auth: "failed"})
	}
})

router.delete("/:courseId", auth.verify, (req, res)=>{
/* Activity:
	Create a route for archiving a specific course with the following specifications:
	1. The course to archive is determined by the ID passed in the URL 
	2. Route must have token verification middleware
	3. Only admins must be allowed to archive courses
	4. Courses are not actually deleted, only their isActive fields changed from true to false
	5. Make sure to use Postman to test
*/
	if (auth.decode(req.headers.authorization).isAdmin){
		courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController))
	}else{
		return res.send({auth: "failed"})
	}
})

module.exports = router












